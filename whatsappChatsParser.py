#!/user/bin/python
import re, json

file = open("chats.txt", "r")

chats = [];
boredList = [];
sleepyList = [];
tiredList = [];
history = {};
count = 0;

for line in file:
  chat = {}
  matchObj = re.match(r'(.*), (.*) - (.*): (.*)', line, re.M|re.I)
  if matchObj:
    chat['date'] = matchObj.group(1);
    chat['time'] = matchObj.group(2);
    chat['sender'] = matchObj.group(3);
    chat['message'] = matchObj.group(4);

    if 'bored' in chat['message']:
      boredList.append(chat)

    if 'sleepy' in chat['message']:
      sleepyList.append(chat)

    if 'tired' in chat['message']:
      tiredList.append(chat)

    count += 1
    chats.append(chat)

history['chats'] = chats
history['boredList'] = boredList
history['sleepyList'] = sleepyList
history['tiredList'] = tiredList
history['count'] = count

with open('messages.json', 'w') as outfile:
  json.dump(history, outfile)

file.close();