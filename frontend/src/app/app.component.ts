import { Component, OnInit } from '@angular/core';
import { ContentService } from './services/content.service';
import { Content } from './objects/content';
import { Chat } from './objects/chats';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  title = 'Bored and Sleepy';
  private content: Content;
  private isLoading: boolean;
  private filteredList: Object;
  private startDate: String;
  private endDate: String;

  constructor(private _contentService: ContentService) { }

  ngOnInit() {
    this.isLoading = false;
    this.getContents();
  }

  getContents(): void {
    this.isLoading = true;
    this._contentService.getContent().subscribe(contents => {
      this.content = contents;
      this.isLoading = false;

      console.log(contents);
      this.getStartEndDate();

      this.filterContentBySender('Elena Dinh', 'bored');
      this.filterContentBySender('Elena Dinh', 'sleepy');
      this.filterContentBySender('Elena Dinh', 'tired');

      this.filterContentBySender('Sandy Phan', 'bored');
      this.filterContentBySender('Sandy Phan', 'sleepy');
      this.filterContentBySender('Sandy Phan', 'tired');

      this.setUpLists();
    });
  }

  filterContentBySender(sender, list): void {
    let mList = [];

    if (!this.content[sender]) {
      this.content[sender] = {};
    }

    let key = '';

    switch (list) {
      case 'bored':
      default:
        mList = this.content.boredList;
        key = 'boredList';
        break;
      case 'sleepy':
        mList = this.content.sleepyList;
        key = 'sleepyList';
        break;
      case 'tired':
        mList = this.content.tiredList;
        key = 'tiredList';
        break;
    }

    this.content[sender][key] = mList.filter(chat => chat.sender === sender);
  }

  setUpLists(): void {

    console.log(this.content);

    console.log(this.content.boredList);


    this.filteredList = {
      'Elena': {
        'bored': {
          'word': 'bored',
          'list': this.content['Elena Dinh'].boredList,
          'count': this.content['Elena Dinh'].boredList.length,
          'sender': 'Elena',
        },
        'sleepy': {
          'word': 'sleepy',
          'list': this.content['Elena Dinh'].sleepyList,
          'count': this.content['Elena Dinh'].sleepyList.length,
          'sender': 'Elena',
        },
        'tired': {
          'word': 'tired',
          'list': this.content['Elena Dinh'].tiredList,
          'count': this.content['Elena Dinh'].tiredList.length,
          'sender': 'Elena',
        }
      },
      'San' : {
        'bored': {
          'word': 'bored',
          'list': this.content['Sandy Phan'].boredList,
          'count': this.content['Sandy Phan'].boredList.length,
          'sender': 'San',
        },
        'sleepy': {
          'word': 'sleepy',
          'list': this.content['Sandy Phan'].sleepyList,
          'count': this.content['Sandy Phan'].sleepyList.length,
          'sender': 'San',
        },
        'tired': {
          'word': 'tired',
          'list': this.content['Sandy Phan'].tiredList,
          'count': this.content['Sandy Phan'].tiredList.length,
          'sender': 'San',
        }
      }
    };
  }

  getStartEndDate(): void {
    const length = this.content.count;
    this.startDate = this.content.chats[0].date;
    this.endDate = this.content.chats[length - 1].date;
  }
}
