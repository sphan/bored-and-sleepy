import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Content } from '../objects/content';

@Injectable()
export class ContentService {


  constructor(private _http: HttpClient) {}

  getContent(): Observable<Content> {
    const url = 'assets/content/messages.json';
    const content = this._http.get<Content>(url);
    return content;
  }
}
