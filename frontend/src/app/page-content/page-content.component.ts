import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'app-page-content',
  templateUrl: './page-content.component.html',
  styleUrls: ['./page-content.component.less']
})
export class PageContentComponent implements OnInit {

  @Input() filteredContent: Object;

  private viewDetailsMode: boolean;

  private detailList: Object;

  constructor() { }

  ngOnInit() {
    this.viewDetailsMode = false;
    this.detailList = [];
  }

  onViewDetailsClick(event) {
    this.viewDetailsMode = true;
    this.detailList = event;
  }

  showOverview(event) {
    this.viewDetailsMode = event;
  }
}
