import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-stat-details',
  templateUrl: './stat-details.component.html',
  styleUrls: ['./stat-details.component.less']
})
export class StatDetailsComponent implements OnInit {

  @Input() content: Object;
  @Input() viewDetailsMode: boolean;

  @Output() backClicked: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {
  }

  onBackClick() {
    this.viewDetailsMode = false;
    this.backClicked.emit(this.viewDetailsMode);
  }

}
