import { Component, OnInit, Input } from '@angular/core';
import { ContentService } from '../services/content.service';
import { Content } from '../objects/content';

@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.less', '../../common.less']
})
export class PageHeaderComponent implements OnInit {

  @Input() contentCount: number;
  @Input() startDate: String;
  @Input() endDate: String;

  constructor(private _contentService: ContentService) { }

  ngOnInit() {}

}
