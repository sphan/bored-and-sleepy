import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-stat-card',
  templateUrl: './stat-card.component.html',
  styleUrls: ['./stat-card.component.less', '../../common.less']
})
export class StatCardComponent implements OnInit {
  @Input() content: Object;
  @Input() sender: string;

  @Output() viewDetailClicked: EventEmitter<Object> = new EventEmitter<Object>();

  private receiverDesc: string;
  private senderDesc: string;

  constructor() { }

  ngOnInit() {
    switch (this.sender) {
      case 'Elena':
        this.senderDesc = 'You';
        this.receiverDesc = 'San';
        break;
      case 'San':
        this.senderDesc = 'San';
        this.receiverDesc = 'You';
        break;
      default:
        break;
    }
  }

  onViewDetailsClick() {
    this.viewDetailClicked.emit(this.content);
  }

}
