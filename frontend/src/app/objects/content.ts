import { Chat } from './chats';

export class Content {
  chats: Chat[];
  boredList: Chat[];
  sleepyList: Chat[];
  tiredList: Chat[];
  count: number;
  // tslint:disable-next-line:eofline
}