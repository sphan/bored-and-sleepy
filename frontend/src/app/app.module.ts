import { BrowserModule } from '@angular/platform-browser';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { MaterialModule } from './material.module';


import { AppComponent } from './app.component';
import { PageHeaderComponent } from './page-header/page-header.component';
import { ContentService } from './services/content.service';
import { PageContentComponent } from './page-content/page-content.component';
import { StatCardComponent } from './stat-card/stat-card.component';
import { StatDetailsComponent } from './stat-details/stat-details.component';
import { MessageComponent } from './message/message.component';


@NgModule({
  declarations: [
    AppComponent,
    PageHeaderComponent,
    PageContentComponent,
    StatCardComponent,
    StatDetailsComponent,
    MessageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MaterialModule,
  ],
  providers: [ContentService],
  bootstrap: [AppComponent]
})
export class AppModule { }
